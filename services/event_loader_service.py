from typing import Any
from injector import inject
from .ship_alert_mapper import ShipAlertMapper


class EventLoaderService:
    @inject
    def __init__(self, mapper: ShipAlertMapper) -> None:
        self._mapper = mapper
        pass

    def load(self, event: Any) -> dict:
        pass
