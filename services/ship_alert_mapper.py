from typing import Optional
from models import ShipAlert


class ShipAlertMapper:
    def map(self, src: dict, target: Optional[ShipAlert]) -> ShipAlert:
        return ShipAlert()
