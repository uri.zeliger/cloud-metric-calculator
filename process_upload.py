from injector import Injector

from services import EventLoaderService, ShipAlertPublisherService
from dal import ShipAlertDal


def lambda_handler(event, context):
    ioc_container = Injector()
    event_loader = ioc_container.get(EventLoaderService)
    publisher = ioc_container.get(ShipAlertPublisherService)
    db = ioc_container.get(ShipAlertDal)
    ship_alert = event_loader.load(event)
    db.insert(ship_alert)
    publisher.publish(ship_alert)
    
